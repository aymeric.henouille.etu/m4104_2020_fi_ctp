package fr.ulille.iutinfo.banque;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public  static final String SOLDES_KEY = "Soldes";

    Spinner donneur;
    Spinner receveur;
    EditText montant;
    EditText operation;

    private String banque;
    private String joueurs[];
    private int soldeInitial;

    private int[] soldes;

    private int selectedDonneur, selectedReceveur;

    private class FlingDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if ((Math.abs(velocityX) > Math.abs(velocityY)) && (velocityX < 0)) {

                // TODO Question 2.6 Lancement de la seconde activité

                // TODO Question 2.7 Communication entre activités

                return true;
            }
            return false;
        }
    }

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO Question 2.1 Chargement des ressources

        banque = getResources().getString(R.string.banque);
        joueurs = getResources().getStringArray(R.array.nom_joueurs);
        soldeInitial = getResources().getInteger(R.integer.solde_initial);

        soldes = new int[]{soldeInitial, soldeInitial, soldeInitial, soldeInitial};

        // TODO Question 2.2 Affichage des noms des joueurs

        TextView joueur1 = findViewById(R.id.joueur1);
        TextView joueur2 = findViewById(R.id.joueur2);
        TextView joueur3 = findViewById(R.id.joueur3);
        TextView joueur4 = findViewById(R.id.joueur4);

        joueur1.setText(joueurs[0]);
        joueur2.setText(joueurs[1]);
        joueur3.setText(joueurs[2]);
        joueur4.setText(joueurs[3]);

        // TODO Question 2.5 Changement de configuration

        donneur = findViewById(R.id.donneur);
        receveur = findViewById(R.id.receveur);
        operation = findViewById(R.id.operation);
        montant = findViewById(R.id.montant);

        // TODO Question 2.3 Initialisation des spinners

        List<String> itemList = new ArrayList<>(Arrays.asList(joueurs));
        itemList.add(banque);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, itemList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        donneur.setAdapter(adapter);
        receveur.setAdapter(adapter);

        donneur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDonneur = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        receveur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedReceveur = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mDetector = new GestureDetectorCompat(this, new FlingDetector());

        update();
    }

    private void update() {
        // TODO Question 2.2 Affichage des soldes des joueurs
        TextView solde1 = findViewById(R.id.solde1);
        TextView solde2 = findViewById(R.id.solde2);
        TextView solde3 = findViewById(R.id.solde3);
        TextView solde4 = findViewById(R.id.solde4);

        solde1.setText(soldes[0] + "");
        solde2.setText(soldes[1] + "");
        solde3.setText(soldes[2] + "");
        solde4.setText(soldes[3] + "");
    }

    public void ajoute_operation(View view) {
        // TODO Question 2.4 ajout d'une transaction

        EditText operationEditText = findViewById(R.id.montant);
        String operationText = operationEditText.getText().toString();

        if (isNumeric(operationText)) {
            double operation = Double.parseDouble(operationText);

            if (selectedReceveur == selectedDonneur) {
                message(getResources().getString(R.string.ajout_operation_ko_cr_db));
                return;
            }

            if (selectedDonneur < 4) {
                if (soldes[selectedDonneur] - operation < 0) {
                    message(getResources().getString(R.string.ajout_operation_ko_solde));
                    return;
                }
                soldes[selectedDonneur] -= operation;
            }

            if (selectedReceveur < 4) {
                soldes[selectedReceveur] += operation;
            }

            operationEditText.setText("0");
            message(getResources().getString(R.string.ajout_operation_ok));

        } else {
            message("Indiquez une valeur numérique pour l'operation.");
            return;
        }



        update();
    }

    // TODO Question 2.5 Changement de configuration

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    private void message(String text) {
        Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
    }

    private boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
